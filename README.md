# fbr

![facebook API](https://adexchanger.com/wp-content/uploads/2011/08/facebook-api.jpg)

Wrapper for facebook API `v2.11`, focuses on GET insights type of functions; package is for analytical purposes.

## Install

Use username and password to install.

```R
devtools::install_git("https://git.weforum.local/jp-handover/fbr")
```

## Functions

*All functions return lists* \* see examples

* `fbr_pideos` - returns `p`age `videos`
* `fbr_vinsights` - returns `v`ideo `insights`
* `fbr_insights` - returns `insights` on object.
* `fbr_feed` - returns feed all feed:
  * `fbr_posts` - returns posts by page
  * `fbr_tagged` - returns public post where page is tagged
  * `fbr_pposts` - returns promotable posts
* `fbr_cta` - returns call to action
* `fbr_flatten` - flattens results to `tibble::tibble` \*
* `fbr_nest` - flattens results to *nested* `tibble::tibble` \*
* `fbr_date`- formats `Date`
* `fbr_time` - format `POSIX`
* `fbr_comments` - get comments

## Examples

```R
token <- "XXxX0X0X0X00Xx0Xx0"

page_id <- 7746841478 # WEF page id

videos <- fbr_pideos(token, page_id) # get list of videos
vids_likes <- fbr_pideos(token, page_id, edge = "likes") # get video likes

# get insights on random video
insights <- fbr_vinsights(token, sample(videos$id, 1), metric = c("total_video_views"))

# get # of facebook fans of WEF
fans <- fbr_insights(token, page_id, metric = "page_fans") %>%
  fbr_frame() # to data.frame

# get yesterday's page video views and impressions by day
insights <- fbr_insights(token, page_id,
  period = "day",
  since = Sys.Date()-1, Sys.Date()-2,
  metric = c("page_video_views", "page_impressions")) %>%
  fbr_flatten() # to data.frame
  
# use purrr & dplyr
library(purrr)
library(dplyr)  
  
# get insights on page posts
posts_data <- fbr_posts(token, page_id) %>% # get posts
  map("id") %>% # extract id
  unlist %>% 
  map(~fbr_insights(token, .x, metric = "post_interests_impressions"))

# get insights on last week
# as list
last_week <- fbr_insights(token,
  page.id = page_id,
  period = "day",
  metric = "page_video_views",
  date.preset = "last_week")

# process with purrr  
last_week %>% 
  map(., ~.x[['values']]) %>% # extract values
  map_df(bind_rows) %>% # build tibble
  mutate(end_time = fbr_time(end_time)) -> withpurrr

# process using built-in flatten function  
last_week[[1]]$values %>% 
  fbr_flatten() %>% 
  mutate(end_time = fbr_time(end_time)) -> withhelpers
```

### NEWS

See `NEWS.md`

-------------------------

Author and Maintainer: <Jean-Philippe.Coene@weforum.org>
