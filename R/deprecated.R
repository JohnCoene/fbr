#' Get page insights
#'
#' Retrives insights on specific page.
#'
#' @param token your page token.
#' @param page.id id of object, see examples.
#' @param n number of videos to fetch.
#' @param metric a list of valid metrics, see details and examples.
#' @param date.preset Preset a date range, like lastweek, yesterday.
#' If since or until presents, it does not work.
#' @param period The aggregation period, takes \code{day}, \code{week}, \code{days_28},
#' \code{month}, \code{lifetime}, \code{total_over_range}.
#' @param since,until lower and upper bounds of the time range to consider.
#' @param ... any other parameter, namely metrics, see details and examples.'
#'
#' @details
#' Advice check the official documentation on metrics, some combiations of metrics/
#' parameters are not allowed, i.e.: "Page Video Views" (see example).
#'
#' Valid \code{date.preset}:
#' \itemize{
#'   \item{\code{today}}
#'   \item{\code{yesterday}}
#'   \item{\code{this_month}}
#'   \item{\code{last_month}}
#'   \item{\code{this_quarter}}
#'   \item{\code{lifetime}}
#'   \item{\code{last_3d}}
#'   \item{\code{last_7d}}
#'   \item{\code{last_14d}}
#'   \item{\code{last_28d}}
#'   \item{\code{last_30d}}
#'   \item{\code{last_90d}}
#'   \item{\code{last_week_mon_sun}}
#'   \item{\code{last_week_sun_sat}}
#'   \item{\code{last_quarter}}
#'   \item{\code{last_year}}
#'   \item{\code{this_week_mon_today}}
#'   \item{\code{this_week_sun_today}}
#'   \item{\code{this_year}}
#' }
#'
#' See \href{https://developers.facebook.com/docs/graph-api/reference/v2.11/page/insights/}{official documentation}
#' for valid parameters.
#' See \href{https://developers.facebook.com/docs/graph-api/reference/v2.11/insights}{official documentation}
#' for valid \code{metric}.
#'
#' @examples
#' \dontrun{
#' token <- "xX0X00x00Xxxx0X0x"
#'
#' # get # of facebook fans of WEF
#' fans <- fbr_pinsights(token, 7746841478, metric = "page_fans") %>%
#'   fbr_flatten() # to data.frame
#'
#' # get yesterday's page video views and impressions by day
#' insights <- fbr_pinsights(token, 7746841478,
#'   period = "day",
#'   since = Sys.Date()-1, Sys.Date()-2,
#'   metric = c("page_video_views", "page_impressions")) %>%
#'   fbr_flatten() # to data.frame
#' }
#'
#' @export
fbr_pinsights <- function(token, page.id, n = 100, metric = NULL,
                         date.preset = NULL, period = NULL, since = NULL,
                         until = NULL, ...){

  .Deprecated("fbr_insights")

  if(missing(token) || missing(page.id) || missing(metric))
    stop("must pass token, page.id and metric.", call. = FALSE)

  if(!is.null(date.preset) && !is.null(since) && !is.null(until))
    warning("ignoring date.preset", call. = FALSE)

  fbr_insights(token, page.id, n, metric, date.preset, period, since, until, ...)
}
