# fbr 0.0.2.9000

* `fields` added to feed-family of funtions

# fbr 0.0.1

* `fbr_pinsights` deprecated in favour of `fbr_insgights`
* `fbr_cta` added
* `fbr_feed` added
* `fbr_pposts` added
* `fbr_posts` added
* `fbr_tagged` added

